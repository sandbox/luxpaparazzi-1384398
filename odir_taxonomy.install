<?php 
/**
 * @file 
 * Install and uninstall taxonomy vocabularies and fields.
 */

/**
 * Implements hook_enable().
 * Inspired by forum_enable().
 */
function odir_taxonomy_enable() {
  odir_field_enable();
  
  // If we enable odir_taxonomy at the same time as taxonomy we need to call
  // field_associate_fields() as otherwise the field won't be enabled until
  // hook modules_enabled is called which takes place after hook_enable events.
  field_associate_fields('taxonomy');
  
  // Create the odir_taxonomy vocabulary if it does not exist.
  $vocabulary = taxonomy_vocabulary_load(variable_get('odir_taxonomy_vocabulary', 0));
  if (!$vocabulary) {
    $edit = array(
      'name' => t('Directories'),
      'machine_name' => 'odir_vocabulary',
      'description' => t('Vocabulary synchronized with directory of the module "Directory based organisational layer".'),
      'hierarchy' => 1,
      'module' => 'odir',
      'weight' => -10,
    );
    $vocabulary = (object) $edit;
    taxonomy_vocabulary_save($vocabulary);
    variable_set('odir_taxonomy_vocabulary', $vocabulary->vid);
  }
  // Create the 'odir_vocabulary_field' field if it doesn't already exist.
  if (!field_info_field('odir_vocabulary_field')) {
    $field = array(
      'field_name' => 'odir_vocabulary_field',
      'type' => 'taxonomy_term_reference',
      'settings' => array(
        'allowed_values' => array(
          array(
            'vocabulary' => $vocabulary->machine_name,
            'parent' => 0,
          ),
        ),
      ),
    );
    field_create_field($field);
  }
  
  $entity_type = 'taxonomy_term';
  $field_name = 'directory';
  $bundle_name = 'odir_vocabulary';
  if (!field_info_instance($entity_type, $field_name, $bundle_name)) {
    // Create the instance on the bundle.
    $instance = array(
      'field_name' => $field_name,
      'entity_type' => $entity_type,
      'label' => 'Directory path of dictionary term',
      'bundle' => $bundle_name,
      'required' => FALSE,
    );
    field_create_instance($instance); 
  }
  drupal_set_message($message = t('Directory taxonomy vocabulary has been initialized.'), $type = 'status');
}

/**
 * Implements hook_uninstall().
 */
function odir_taxonomy_uninstall() {
  taxonomy_vocabulary_delete(variable_get('odir_taxonomy_vocabulary'));
  field_delete_field('odir_vocabulary_field');
  db_delete('variable')->condition('name', 'odir_taxonomy_%', 'LIKE')->execute();
}
