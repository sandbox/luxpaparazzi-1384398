<?php
/**
 * @file
 * This module provides a node type called lan event.
 */

/**
 * Implemenats hook_help().
 */
function odir_taxonomy_help($path, $arg) {
  if ($path == 'admin/help#odir_taxonomy') {
    $txt  = "Associating taxonomy term to directory.";
    $replace = array();
    return '<p>' . t($txt, $replace) . '</p>';
  }
}

/**
 * Get taxonomy term by it's path
 */
function odir_taxonomy_search_by_path($path) {
  $result = db_query('SELECT tid,  name                      
                      FROM {taxonomy_term_data} tt
                      LEFT JOIN {field_data_directory} f
                      ON tt.tid = f.entity_id
                      WHERE tt.vid = :vid
                      AND directory_value = :path
                      ', array(':vid' => variable_get('odir_taxonomy_vocabulary'),
                               ':path' => $path,  
                              )
                    );
  foreach ($result as $record) {
    return ($record);
  }
  return NULL;
}

/**
 * Creates taxonomy terms for directory and it's parents
 */
function _odir_taxonomy_update_database($dir, $parent_count=0) {
  if ($dir == "") {
    return;
  }
  if ($parent_count > 200) {
    die("_odir_taxonomy_update_database to many parents");
  }
  $term_data = odir_taxonomy_search_by_path($dir);
  if ($term_data) {
    return $term_data->tid;
  }
  else {
    if ($dir != "/") {
      $parent_dir = odir_get_parent($dir);
      $parent_tid = _odir_taxonomy_update_database($parent_dir, $parent_count+1);
    }
    else {
      $parent_tid = 0;
    }
    $edit = array(
      'name' => odir_short_filename($dir),
      'description' => '',
      'parent' => array($parent_tid),
      'vid' => variable_get('odir_taxonomy_vocabulary'),
    );
    $edit['directory']['und'][0]['value'] = $dir;
    $term = (object) $edit;
    //print "<pre>";print_r($term);print "</pre>";
    taxonomy_term_save($term);
    $tid = _odir_taxonomy_update_database($dir, $parent_count);
    return $tid;
  }
}

/**
 * Implements hook_odir_directory_pre_processing().
 * Updates taxonomy terms if necessary.
 */
function odir_taxonomy_odir_directory_pre_processing($dir) {
  _odir_taxonomy_update_database($dir);
}

/**
 * Implements hook_form_alter().
 * 
 * Altering behavior of taxonomy_overview_terms.
 * Thanks to rubenski for his post @ http://drupal.org/node/1135004
 */
function odir_taxonomy_form_alter(&$form, &$form_state, $form_id) { 
    if($form_id == "taxonomy_overview_terms"){
        $form['#submit'][] = 'odir_taxonomy_overview_terms_extra';  
    }
}

/**
 * Copy of taxonomy_overview_terms_submit()
 * for passing changed terms to odir_taxonomy_vocabulary_update().
 */
function odir_taxonomy_overview_terms_extra($form, &$form_state) {
  $vocabulary = $form['#vocabulary'];
  $hierarchy = 0; // Update the current hierarchy type as we go.
  
  $changed_terms = array();
  $tree = taxonomy_get_tree($vocabulary->vid);
  if (empty($tree)) {
    return;
  }
  if ($tree[0]->vid != variable_get('odir_taxonomy_vocabulary')) {
    return;
  } 


  // Build a list of all terms that need to be updated on previous pages.
  $weight = 0;
  $term = (array) $tree[0];
  while ($term['tid'] != $form['#first_tid']) {
    if ($term['parents'][0] == 0 && $term['weight'] != $weight) {
      $term['parent'] = $term['parents'][0];
      $term['weight'] = $weight;
      $changed_terms[$term['tid']] = $term;
    }
    $weight++;
    $hierarchy = $term['parents'][0] != 0 ? 1 : $hierarchy;
    $term = (array) $tree[$weight];
  }
  // Renumber the current page weights and assign any new parents.
  $level_weights = array();
  foreach ($form_state['values'] as $tid => $values) {
    if (isset($form[$tid]['#term'])) {
      $term = $form[$tid]['#term'];
      $term['parent_old'] = $term['parent'];
  
      // Update any changed parents.
      if ($values['parent'] != $term['parent']) {
        $term['parent'] = $values['parent'];
        $changed_terms[$term['tid']] = $term;
      }
      $hierarchy = $term['parent'] != 0 ? 1 : $hierarchy;
      $weight++;
    }
  }

  // Save all updated terms.
  foreach ($changed_terms as $changed) {
    $term = (object) $changed;
    odir_taxonomy_vocabulary_update($term);
  }
}

/**
 * Implements hook_term_insert().
 */
function odir_taxonomy_term_insert($term) {
  if ($term->vid != variable_get('odir_taxonomy_vocabulary')) return;
}

/**
 * Implements hook_term_update().
 */
function odir_taxonomy_term_update($term) {
  if ($term->vid != variable_get('odir_taxonomy_vocabulary')) return;
}

/**
 * Implements hook_term_delete().
 */
function odir_taxonomy_term_delete($term) {
  if ($term->vid != variable_get('odir_taxonomy_vocabulary')) return;
}

/**
 * Implements hook_vocabulary_insert().
 */
function odir_taxonomy_vocabulary_insert($term) {
  if ($term->vid != variable_get('odir_taxonomy_vocabulary')) return;
}

/**
 * Implements hook_vocabulary_update().
 * 
 * @todo
 */
function odir_taxonomy_vocabulary_update($term) {
  if ($term->vid != variable_get('odir_taxonomy_vocabulary')) return;
  if ($term->parent != $term->parent_old) {
    $term_parent_old = taxonomy_term_load($term->parent_old);
    $term_parent_old_path = $term_parent_old->directory['und'][0]['value'];
    
    $term_parent_new = taxonomy_term_load($term->parent_old);
    $term_parent_new_path = $term_parent_new->directory['und'][0]['value'];
    $directory_new_uri = 'odir://'.$term_parent_new_path;
    $directory_old_uri = 'odir://'.$term_parent_old_path;
    odir_move($directory_old_uri, $directory_new_uri);
  }
}

/**
 * Implements hook_vocabulary_delete().
 */
function odir_taxonomy_vocabulary_delete($term) {
  if ($term->vid != variable_get('odir_taxonomy_vocabulary')) return;
}
